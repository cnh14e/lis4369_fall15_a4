﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment4
{
    public class Program
    {
        public void Main(string[] args)
        {

            Console.WriteLine("A4 - Inheritance");
            Console.WriteLine("Author: Cuong Huynh");
            Console.WriteLine("Now: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));
            Console.WriteLine("    ");

            //base person object
            Console.WriteLine("Creating base person object from default constructor (accepts no arguments): ");
            Person defaultConstructor = new Person();
            Console.WriteLine("First Name: " + defaultConstructor.GetFname());
            Console.WriteLine("Last Name: " + defaultConstructor.GetLname());
            Console.WriteLine("Age: " + defaultConstructor.GetAge());
            Console.WriteLine();


            //USING SET/GET
            Console.WriteLine("Modify person object's data member values created from default constructor: ");
            Console.WriteLine("User setter/getter method: ");
            Console.Write("First Name: ");
            string personFname = Console.ReadLine();
            defaultConstructor.SetFname(personFname);


            Console.Write("Last Name: ");
            string personLname = Console.ReadLine();
            defaultConstructor.SetLname(personLname);

            int whileAge;
            Console.Write("Age: ");
            while (!int.TryParse(Console.ReadLine(), out whileAge))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("Age: ");
            }
            defaultConstructor.SetAge(whileAge);


            //DISPLAY SET/GET
            Console.WriteLine();
            Console.WriteLine("Display object's new data member values: ");


            Console.Write("First Name: ");
            Console.WriteLine(defaultConstructor.GetFname());
            Console.Write("Last Name: ");
            Console.WriteLine(defaultConstructor.GetLname());
            Console.Write("Age: ");
            Console.WriteLine(defaultConstructor.GetAge());
            Console.WriteLine();



            //call parameterized constructor
            Console.WriteLine("Call parameterized base constructor (accepts arguments): ");
            Console.Write("First Name: ");
            string paramFname = Console.ReadLine();
            Console.Write("Last Name: ");
            string paramLname = Console.ReadLine(); ;
            Console.Write("Age: ");
            Console.Write("");
            int paramAge;
            while (!int.TryParse(Console.ReadLine(), out paramAge))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("Age: ");
                Console.WriteLine("  ");
            }

            Console.WriteLine();


            //parameterized cons.
            Console.Write("Creating base person object from parameterized constructor (accepts arguments): ");
            Person paramConstructor = new Person(paramFname, paramLname, paramAge);
            Console.WriteLine("First Name: " + paramConstructor.GetFname());
            Console.WriteLine("Last Name: " + paramConstructor.GetLname());
            Console.WriteLine("Age: " + paramConstructor.GetAge());
            Console.WriteLine();

            Console.WriteLine("Call derived default constructor (inherits from base class):");
            Console.Write("***Note***: Because derived default student constructor does not call ");
            Console.WriteLine("base class explicitly, default constructor in base class called implicitly");
            Console.WriteLine();
            Console.WriteLine("That is why, here, base class *must* contain default constructor!");
            Console.WriteLine();

            Console.WriteLine("Creating base person object from default constructor (accepts no arguments): ");
            Console.Write("Creating derived student object from default constructor (accepts no arguments): ");
            Person person = new Person();
            Console.WriteLine("First Name: " + person.GetFname());
            Console.WriteLine("Last Name: " + person.GetLname());
            Console.WriteLine("Age: " + person.GetAge());
            Console.WriteLine();

            Console.WriteLine("Demonstrating Polymorphism (new derived object): ");
            Console.WriteLine("(Calling parameterized base class constructor explicity.)");
            Console.WriteLine();
            Console.Write("First Name: ");
            string polyFname = Console.ReadLine();

            Console.Write("Last Name: ");
            string polyLname = Console.ReadLine();

            Console.Write("Age: ");
            int polyAge;
            while (!int.TryParse(Console.ReadLine(), out polyAge))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("Age: ");
            }

            Console.Write("College: ");
            string polyCollege = Console.ReadLine();

            Console.Write("Major : ");
            string polyMajor = Console.ReadLine();

            Console.Write("GPA: ");
            double polyGPA;
            while (!double.TryParse(Console.ReadLine(), out polyGPA))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("GPA: ");
                Console.WriteLine("  ");
            }

            //Student paramStudent = new Student(polyFname, polyLname, polyAge, polyCollege, polyMajor, polyGPA);

            Console.WriteLine();
            Console.WriteLine("Creating base person object from paramterized constructor (accepts arguments): ");
            Console.WriteLine("Creating derived student object from parameterized constructor (accepts arguments): ");
            Console.WriteLine();

            Person person2 = new Person(paramFname, paramLname, paramAge);
            
            Console.WriteLine("person2 - GetObjectInfo (virtual): ");
            Console.WriteLine(person2.GetFname() + " " + person2.GetLname() + " is " + person2.GetAge());


            Student student2 = new Student(polyFname, polyLname, polyAge, polyCollege, polyMajor, polyGPA);
            Console.WriteLine("student2 - GetObjectInfo (overidden): ");
            Console.Write(student2.GetFname() + " " + student2.GetLname() + " is " + student2.GetAge() + " in the college of ");
            Console.WriteLine(student2.GetCollege() + ", majoring in " + student2.GetMajor() + ", and has a " + student2.GetGPA() + " gpa.");

            // Student student2 = new Student();
            // Console.WriteLine("")

            Console.WriteLine("  ");
            Console.WriteLine("Press Enter to exit ...");
            Console.WriteLine("  ");
            Console.ReadKey(true);

        }
    }
}
