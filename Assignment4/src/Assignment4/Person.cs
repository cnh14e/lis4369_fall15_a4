﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment4
{
    public class Person
    {
        private string fname;
        private string lname;
        private int age;

        public int GetAge()
        {
            return age;
        }

        public void SetAge(int type = 0)
        {
            age = type;
        }

        public string GetFname()
        {
            return fname;
        }

        public void SetFname(string type = "First name")
        {
            fname = type;
        }


        public string GetLname()
        {
            return lname;
        }

        public void SetLname(string type = "Last name")
        {
            lname = type;
        }
        public Person()
        {
            fname = "First name";
            lname = "Last name";
            age = 0;
            /*
            Console.WriteLine("First Name: " + this.fname);
            Console.WriteLine("Last Name: " + this.lname);
            Console.WriteLine("Age: " + this.age);
            Console.WriteLine();
            */
        }
        public Person(string fn = "First name", string ln = "Last name", int consAge = 0)
        {
            fname = fn;
            lname = ln;
            age = consAge;
            /*
            Console.WriteLine("First Name: " + this.fname);
            Console.WriteLine("Last Name: " + this.lname);
            Console.WriteLine("Age: " + this.age);
            Console.WriteLine();
            */
        }

        public class Student : Person
        {

            private string fname;
            private string lname;
            private int age;
            private string college;
            private string major;
            private double gpa;


            public int GetAge()
            {
                return age;
            }

            public void SetAge(int type = 0)
            {
                age = type;
            }

            public string GetCollege()
            {
                return college;
            }

            public void SetCollege(string type = "")
            {
                college = type;
            }
            public string GetMajor()
            {
                return major;
            }

            public void SetMajor(string type = "")
            {
                major = type;
            }
            public double GetGPA()
            {
                return gpa;
            }

            public void SetGPA(double type = 0.0)
            {
                gpa = type;
            }

            public string GetFname()
            {
                return fname;
            }

            public void SetFname(string type = "First name")
            {
                fname = type;
            }


            public string GetLname()
            {
                return lname;
            }

            public void SetLname(string type = "Last name")
            {
                lname = type;
            }
            public Student()
            {
                fname = "First name";
                lname = "Last name";
                age = 0;
                /*
                Console.WriteLine("First Name: " + this.fname);
                Console.WriteLine("Last Name: " + this.lname);
                Console.WriteLine("Age: " + this.age);
                */
                Console.WriteLine();
            }
            public Student(string fn = "First name", string ln = "Last name", int consAge = 0, string col = "", string maj = "", double consGPA = 0.0)
            {
                fname = fn;
                lname = ln;
                age = consAge;
                college = col;
                major = maj;
                gpa = consGPA;

                Console.WriteLine();
                /*
                Console.WriteLine("First Name: " + this.fname);
                Console.WriteLine("Last Name: " + this.lname);
                Console.WriteLine("Age: " + this.age);
                Console.WriteLine("College: " + this.college);
                Console.WriteLine("Major : " + this.major);
                Console.WriteLine("GPA: " + this.gpa);
                */
                Console.WriteLine();
            }

        }
    }
}
