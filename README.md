# LIS4369 FALL15 A4
## Cuong Huynh

==============================================

## Requirements

### Make the following changes:

*    In “Running the Application” change
ViewBag.Message = "ASP.NET 5 Rocks!";
to
ViewData["Message"] = "Assignment 4 - My First ASP.NET 5
Application.";

*    In “Adding Server-Side Behavior”
i. Rt-click on WebApplication name > add > new folder (ViewModels)
ii. Rt-click on ViewModels folder > add > class (select Class serverside),
name it ViewModel.cs > Add > then add code from tutorial

*    In “Adding Server-Side Behavior” change
HomeController’s About method
string appName = "Your First ASP.NET 5 App";
ViewBag.Message = "Your Application Name: " + appName;
to
string appName = "My First ASP.NET 5 Application.";
ViewData["Message"] = "Assignment 4 - " + appName;

*    Also, don’t forget to include the following using statement:
using WebApplication2.ViewModels;
*    In project.json, change “frameworks” to
 "frameworks": {
 "dnx451": { }
 },
 
*    Be sure that the following using statement is included in
ProcessesController.cs file
using System.Diagnostics;


==============================================

## Screenshots for Project 1

[![Screenshot 1](images/a4.png)](images/a4.png)
[![Screenshot 1](images/a4-2.png)](images/a4-2.png)

==============================================

## Links
[Bitbucket Repo](https://cnh14e@bitbucket.org/cnh14e/lis4369_fall15_a4)  

